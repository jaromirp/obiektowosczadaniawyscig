package ZadanieWyscig;

import java.util.Random;

public class Zawodnik {

    String imie;
    private int identyfikator;
    private int predkoscMinimalna;
    private int predkoscMaksymalna;
    private int pokonanaOdleglosc;

    public Zawodnik(String imie, int identyfikator, int predkoscMinimalna, int predkoscMaksymalna) {
        this.imie = imie;
        this.identyfikator = identyfikator;
        this.predkoscMinimalna = predkoscMinimalna;
        this.predkoscMaksymalna = predkoscMaksymalna;
    }

    public int getPokonanaOdleglosc() {
        return pokonanaOdleglosc;
    }

    void przedstawSie() {

        System.out.println(String.format("Mam na imię %s, mój numer to %d#, biegam z prędkością od %d do %d", imie,
                identyfikator, predkoscMinimalna, predkoscMaksymalna));

    }


    public void biegnij() {

        Random random = new Random();
        pokonanaOdleglosc += predkoscMinimalna+random.nextInt(predkoscMaksymalna-predkoscMinimalna); //generuje od 1 do 10, bo od 0-9, ale zawsze "+1" dlatego nigdy 0, a max 10 .

    }


}