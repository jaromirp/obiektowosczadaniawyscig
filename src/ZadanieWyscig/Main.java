package ZadanieWyscig;

public class Main {

    public static void main(String[] args) {

        Zawodnik pawel = new Zawodnik("Paweł", 7, 7, 23);

        Zawodnik maciek = new Zawodnik("Maciek", 2, 4, 27);

        Zawodnik michal = new Zawodnik("Michal", 13, 2, 17);

        Zawodnik[] zawodnicy = new Zawodnik[]{pawel, maciek, michal};

        Zawodnik zwyciezca = null;

        do {
            for (Zawodnik zawodnik : zawodnicy) {
                zawodnik.biegnij();
                if (zawodnik.getPokonanaOdleglosc() >= 10000) {
                    zwyciezca = zawodnik;
                    break;
                }
            }
        } while (zwyciezca == null);

        System.out.println("Zwyciezyl " + zwyciezca.imie);
        zwyciezca.przedstawSie();

    }

}
